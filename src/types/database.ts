import { Database } from "../../database.types";

export type Music = Database["public"]['Tables']['Musics']['Row']