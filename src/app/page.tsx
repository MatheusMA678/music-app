import { AudioPlayer } from "@/components/AudioPlayer"
import { supabase } from "@/services/supabase"

export default async function Home() {
  const { data } = await supabase.from('Musics').select('*')

  return (
    <div className="flex h-screen items-center justify-center">
      {data?.map(music => {
        return (
          <AudioPlayer key={music.id} music={music} />
        )
      })}
    </div>
  )
}
