"use client"

import { Music } from "@/types/database"
import { HTMLInputTypeAttribute, InputHTMLAttributes, useEffect, useRef, useState } from "react"
import { Progress } from "./ui/progress"
import { Pause, Play } from "lucide-react"
import { Slider } from "./ui/slider"
import Image from "next/image"

interface AudioPlayerProps {
  music: Music
}

export function AudioPlayer({ music }: AudioPlayerProps) {
  const [isPlaying, setIsPlaying] = useState(false)
  const [duration, setDuration] = useState(0)
  const [currentTime, setCurrentTime] = useState(0)

  const audioRef = useRef<HTMLAudioElement | null>(null)
  const progressbarRef = useRef<HTMLInputElement | null>(null)
  const animationRef = useRef<number | null>(null)

  const togglePlayPause = () => {
    const prevValue = isPlaying
    setIsPlaying(!prevValue)

    if (!prevValue) {
      audioRef.current?.play()
      animationRef.current = requestAnimationFrame(whilePlaying)
    } else {
      audioRef.current?.pause()
      cancelAnimationFrame(animationRef.current!)
    }
  }

  const whilePlaying = () => {
    if (progressbarRef.current) {
      progressbarRef.current.value = String(audioRef.current?.currentTime)

      changePlayerCurrentTime()
      animationRef.current = requestAnimationFrame(whilePlaying)
    }
  }

  const changeRange = () => {
    if (audioRef.current && progressbarRef.current) {
      audioRef.current.currentTime = Number(progressbarRef.current?.value)

      changePlayerCurrentTime()
    }
  }

  const changePlayerCurrentTime = () => {
    if (audioRef.current && progressbarRef.current) {
      progressbarRef.current.style.setProperty("--seek-before-width", `${Number(progressbarRef.current.value) / duration * 100}%`)

      setCurrentTime(Number(progressbarRef.current.value))
    }
  }

  const calculateTime = (secs: number) => {
    const minutes = Math.floor(secs / 60)
    const returnedMinutes = minutes < 10 ? `0${minutes}` : minutes

    const seconds = Math.floor(secs % 60)
    const returnedSeconds = seconds < 10 ? `0${seconds}` : seconds

    return `${returnedMinutes}:${returnedSeconds}`
  }

  useEffect(() => {
    const seconds = Math.floor(audioRef.current?.duration!)
    setDuration(seconds)

    if (progressbarRef.current) {
      progressbarRef.current.max = String(seconds)
    }
  }, [audioRef.current?.onloadedmetadata, audioRef.current?.readyState])

  return (
    <div className="shadow-lg bg-foreground/70 text-zinc-100 rounded-2xl p-6 flex flex-col gap-2 items-center">
      <Image
        src={music.cover_img}
        alt="Music poster"
        width={250}
        height={250}
        className="object-cover rounded-lg"
      />

      <h1>{music.name}</h1>

      <div className="flex w-full gap-2 items-center">
        <span className="block w-12 text-center">{calculateTime(currentTime)}</span>

        <input ref={progressbarRef} type="range" defaultValue={0} onChange={changeRange} className="flex-1 appearance-none bg-zinc-800 rounded-lg cursor-pointer" />

        <span className="block w-12 text-center">{(duration && !isNaN(duration)) && calculateTime(duration)}</span>
      </div>

      {!isPlaying ? (
        <button
          className="h-8 w-8 rounded-full bg-background text-zinc-950"
          onClick={togglePlayPause}
        >
          <Play size={16} fill="black" className="mx-auto" />
        </button>
      ) : (
        <button
          className="h-8 w-8 rounded-full bg-zinc-200 text-zinc-950"
          onClick={togglePlayPause}
        >
          <Pause size={16} fill="black" className="mx-auto" />
        </button>
      )}

      <audio
        ref={audioRef}
        src={music.audio_url}
      />
    </div>
  )
}
