export type Json =
  | string
  | number
  | boolean
  | null
  | { [key: string]: Json | undefined }
  | Json[]

export interface Database {
  public: {
    Tables: {
      Musics: {
        Row: {
          audio_url: string
          cover_img: string
          created_at: string
          id: number
          name: string
          release_date: string
        }
        Insert: {
          audio_url?: string
          cover_img?: string
          created_at?: string
          id?: number
          name?: string
          release_date?: string
        }
        Update: {
          audio_url?: string
          cover_img?: string
          created_at?: string
          id?: number
          name?: string
          release_date?: string
        }
        Relationships: []
      }
    }
    Views: {
      [_ in never]: never
    }
    Functions: {
      [_ in never]: never
    }
    Enums: {
      [_ in never]: never
    }
    CompositeTypes: {
      [_ in never]: never
    }
  }
}